﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IssueCREandTrackStatus.BLL;
using IssueCREandTrackStatus.Contracts.Models;
using IssueCREandTrackStatus.Contracts.BLL;

namespace IssueCREandTrackStatus.Data
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerOrdersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ICustOrder _CustOrder;

        public CustomerOrdersController(ApplicationDbContext context, ICustOrder CustOrder)
        {
            _context = context;
            _CustOrder = CustOrder;
        }

        // GET: api/CustomerOrders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustOrderd>>> GetCustOrderd()
        {
            return await _context.CustOrderd.ToListAsync();
        }

        // GET: api/CustomerOrders/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CustOrderd>> GetCustOrderd(string id)
        {
            var custOrderd = _context.CustOrderd.Where(c => c.CustOrderId == id).FirstOrDefault();

            if (custOrderd == null)
            {
                return NotFound();
            }

            return custOrderd;
        }

        // PUT: api/CustomerOrders/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCustOrderd(string id, CustOrderd custOrderd)
        {
            if (id != custOrderd.CustId)
            {
                return BadRequest();
            }

            _context.Entry(custOrderd).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CustOrderdExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CustomerOrders
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CustOrderd>> PostCustOrderd(CustOrderd custOrderd)
        {
            CustOrderd custOrderd_m =_context.CustOrderd.Find(custOrderd.CustOrderId);
            custOrderd_m.ExchangeStatus = custOrderd.ExchangeStatus;
            custOrderd_m.CreditStatus = custOrderd.ExchangeStatus;
            try
            {
                 _context.SaveChanges();
            }
            catch (Exception ex)
            {
               string msg= ex.Message;
            }
          
            return CreatedAtAction("GetCustOrderd", new { id = custOrderd.CustId }, custOrderd);
        }

        // DELETE: api/CustomerOrders/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CustOrderd>> DeleteCustOrderd(string id)
        {
            var custOrderd = await _context.CustOrderd.FindAsync(id);
            if (custOrderd == null)
            {
                return NotFound();
            }

            _context.CustOrderd.Remove(custOrderd);
            await _context.SaveChangesAsync();

            return custOrderd;
        }

        private bool CustOrderdExists(string id)
        {
            return _context.CustOrderd.Any(e => e.CustId == id);
        }
    }
}
