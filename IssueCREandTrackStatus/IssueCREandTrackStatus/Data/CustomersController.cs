﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IssueCREandTrackStatus.Contracts.Models;
using IssueCREandTrackStatus.Contracts.BLL;
using IssueCREandTrackStatus.BLL;
using Microsoft.AspNetCore.Authorization;

namespace IssueCREandTrackStatus.Data
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustomersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ICustomer _customer;

        public CustomersController(ApplicationDbContext context, ICustomer customer)
        {
            _context = context;
            _customer = customer;
        }

       
        // GET: api/Customers
        //[HttpGet]
        [Authorize(Roles = "User")]
        [HttpGet, Route("GetCustomer")]
        public async Task<ActionResult<IEnumerable<Customer>>> GetCustomer()
        {
            var customers = _customer.GetCustomerDetails();
            return customers;
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Customer>> GetCustomer(string id)
        {
            var customer =  _customer.GetCustomer(id);
            if (customer == null)
            {
                return NotFound();
            }
            return customer;
        }

        //// PUT: api/Customers/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutCustomer(string id, Customer customer)
        //{
        //    if (id != customer.CustId)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(customer).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CustomerExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        //// POST: api/Customers
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPost]
        //public async Task<ActionResult<Customer>> PostCustomer(Customer customer)
        //{
        //    _context.Customer.Add(customer);
        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateException)
        //    {
        //        if (CustomerExists(customer.CustId))
        //        {
        //            return Conflict();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return CreatedAtAction("GetCustomer", new { id = customer.CustId }, customer);
        //}

        // DELETE: api/Customers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCustomer(string id)
        {
            var customer = _customer.DeleteCustomer(id);
            if (customer == false)
            {
                return NotFound();
            }
            else {
                return Ok();
            }
        }

        //private bool CustomerExists(string id)
        //{
        //    return _context.Customer.Any(e => e.CustId == id);
        //}
    }
}
