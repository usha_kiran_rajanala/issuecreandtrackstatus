﻿using IssueCREandTrackStatus.Contracts.BLL;
using IssueCREandTrackStatus.Contracts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Data
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CustOrderController : ControllerBase
    {
        private readonly ICustOrder _CustOrder;

        public CustOrderController(ICustOrder CustOrder)
        {
            _CustOrder = CustOrder;
        }
        // GET: api/Customers
        [Authorize(Roles = "Admin")]
        [HttpGet, Route("GetCustOrder")]
        public async Task<ActionResult<CustOrderd>> GetCustOrder(string CustomerID)
        {
            var customers = _CustOrder.GetCustometOrder(CustomerID, null);
            if (customers != null)
            {
                return customers;
            }
            else {

                return NotFound();
            }
        }
    }
}
