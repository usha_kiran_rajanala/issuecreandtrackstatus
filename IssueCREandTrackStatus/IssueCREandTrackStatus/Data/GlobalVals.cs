﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Data
{
    public class GlobalVals
    {
        public string token { get; set; }
        public string UserName { get; set; }
        public string NotAuthorized { get; set; }
    }
}
