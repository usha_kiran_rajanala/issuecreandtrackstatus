﻿using IssueCREandTrackStatus.BLL;
using IssueCREandTrackStatus.Contracts.BLL;
using IssueCREandTrackStatus.Contracts.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace IssueCREandTrackStatus.Data
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IUsers _users;

        public UsersController(ApplicationDbContext context, IUsers users)
        {
            _context = context;
            _users = users;
        }
        [HttpGet, Route("GetUsers")]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<IEnumerable<Users>>> GetUsers()
        {
            var users = _users.GetUsers();
            return users;
        }
        [HttpPost, Route("GetAuthenticate")]
        public async Task<ActionResult<AuthResult>> GetAuthenticate([FromBody] Users user)
        {
            AuthResult auth = new AuthResult();
            var users = _users.GetAuthenticate(user.LoginId, user.Password);
            if (users != null)
            {
                var roles = _users.GetRole(users.RoleId);
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.LoginId.ToString()),
                    new Claim(ClaimTypes.Role, roles.RoleName)
                    }),
                    //Expires = DateTime.Now.AddMinutes(1),
                    Expires = DateTime.Now.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                try
                {
                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    auth.Token = tokenHandler.WriteToken(token);
                }
                catch(Exception ex)
                {
                    auth.Token ="";
                }
               
                //user.Token = tokenHandler.WriteToken(token);
            }
            return auth;
        }
    }
}
