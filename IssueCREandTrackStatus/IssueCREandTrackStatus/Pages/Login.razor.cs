﻿using IssueCREandTrackStatus.Contracts.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Pages
{
    public partial class Login
    {
        private IssueCREandTrackStatus.Data.LoginModel loginModel = new IssueCREandTrackStatus.Data.LoginModel();
        private bool loading = false;
        private async Task OnValidSubmit()
        {
            IssueCREandTrackStatus.Contracts.Models.Users user = new IssueCREandTrackStatus.Contracts.Models.Users();
            user.LoginId = loginModel.Email;
            user.Password = loginModel.Password;

            string postData = JsonConvert.SerializeObject(user);
            StringContent stringContent = new StringContent(postData);
            var inputMessage = new HttpRequestMessage
            {
                Content = new StringContent(postData, System.Text.Encoding.UTF8, "application/json")
            };
            inputMessage.Headers.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var message = await Http.PostAsync("/api/Users/GetAuthenticate/", inputMessage.Content);
            if (message.IsSuccessStatusCode)
            {
                string strResponse = message.Content.ReadAsStringAsync().Result;
                AuthResult token = JsonConvert.DeserializeObject<IssueCREandTrackStatus.Contracts.Models.AuthResult>(strResponse);
                localstorage.token = token.Token;
                localstorage.UserName= loginModel.Email;
            }
            _navigationManager.NavigateTo("/blazorattributes");
        }
    }
}
