﻿using IssueCREandTrackStatus.Data;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Pages
{
    public partial class BlazorAttributes
    {
        protected string Coordinates { get; set; }

        protected void Mouse_Move(MouseEventArgs e)
        {
            Coordinates = $"X = {e.ClientX } Y = {e.ClientY}";
        }

        private UserInformation model = new UserInformation();
        private EditContext editContext;

        protected override void OnInitialized()
        {
            editContext = new EditContext(model);
        }

        private void HandleValidSubmit()
        {
            var modelJson = JsonSerializer.Serialize(model, new JsonSerializerOptions { WriteIndented = true });
        }

        private void HandleReset()
        {
            model = new UserInformation();
            editContext = new EditContext(model);
        }

    }
}
