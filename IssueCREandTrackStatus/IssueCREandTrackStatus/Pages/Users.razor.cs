﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Pages
{
    public partial class Users
    {
        private IssueCREandTrackStatus.Contracts.Models.Users[] users;
        protected override async Task OnInitializedAsync()
        {
            string jwttoken = localstorage.token;
            var req = new HttpRequestMessage(HttpMethod.Get, "/api/Users/GetUsers");
            req.Headers.Authorization = new AuthenticationHeaderValue("Bearer", jwttoken);
            using var response = await Http.SendAsync(req);
            if (response.IsSuccessStatusCode)
            {
                string strResponse = response.Content.ReadAsStringAsync().Result;
                users = JsonConvert.DeserializeObject<IssueCREandTrackStatus.Contracts.Models.Users[]>(strResponse);
            }
            else if (response.StatusCode.ToString() == "Forbidden")
            {
                UriHelper.NavigateTo("/unauthorized");

            }
        }
    }
}
