﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IssueCREandTrackStatus.Contracts.BLL;

namespace IssueCREandTrackStatus.Controlers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomer _customer;

        public CustomerController(ICustomer customer)
        {
            _customer = customer;
        }

        [HttpGet]
        public List<IssueCREandTrackStatus.Contracts.Models.Customer> GetCutomer()
        {
            return _customer.GetCustomerDetails();
        }
    }
}
