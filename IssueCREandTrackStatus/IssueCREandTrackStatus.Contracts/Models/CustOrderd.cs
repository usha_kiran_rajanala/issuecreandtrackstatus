﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace IssueCREandTrackStatus.Contracts.Models
{
   public class CustOrderd
    {
      
        
        [StringLength(20,ErrorMessage ="Lenght should not exceed from 20")]
        public string CustId { get; set; }
      
        [StringLength(100, ErrorMessage = "Lenght should not exceed from 100")]
        public string CustName { get; set; }
        [Key]
        [StringLength(30, ErrorMessage = "Lenght should not exceed from 30")]
        public string CustOrderId { get; set; }

        [StringLength(50, ErrorMessage = "Lenght should not exceed from 50")]
        public string ProductType { get; set; }

        [StringLength(5, ErrorMessage = "Lenght should not exceed from 5")]
        public string Quantity { get; set; }

        [StringLength(2, ErrorMessage = "Lenght should not exceed from 2")]
        public string CreditStatus { get; set; }

        [StringLength(2, ErrorMessage = "Lenght should not exceed from 2")]
        public string ExchangeStatus { get; set; }

        [StringLength(50, ErrorMessage = "Lenght should not exceed from 50")]
        public string InvoiceAmount { get; set; }

    }


    public class CustOrderdVM
    {
         public string CustId { get; set; }

        public string CustName { get; set; }
        public string CustOrderId { get; set; }

        public string ProductType { get; set; }

        public string Quantity { get; set; }

        public string CreditStatus { get; set; }

        public string ExchangeStatus { get; set; }
        public string InvoiceAmount { get; set; }

        public string ReturnAmount { get; set; }
        public string ApproveStatus { get; set; }

    }
}
