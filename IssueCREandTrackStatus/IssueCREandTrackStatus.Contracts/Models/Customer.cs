﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IssueCREandTrackStatus.Contracts.Models
{
    public class Customer
    {
        [Key]
        public string CustId { get; set; }
        public string CustName { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public string InsertBy { get; set; }

    }
}
