﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IssueCREandTrackStatus.Contracts.Models
{
   public  class Users
    {
        [Key]
        public Int32 UserId { get; set; }
        public string UserName { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string UserAddress { get; set; }
        public string UserEmail { get; set; }
        public Int32 RoleId { get; set; }
    }
}
