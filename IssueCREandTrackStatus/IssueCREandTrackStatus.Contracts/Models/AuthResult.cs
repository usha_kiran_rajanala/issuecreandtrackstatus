﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IssueCREandTrackStatus.Contracts.Models
{
    public class AuthResult
    {
        public string Token { get; set; }
    }
}
