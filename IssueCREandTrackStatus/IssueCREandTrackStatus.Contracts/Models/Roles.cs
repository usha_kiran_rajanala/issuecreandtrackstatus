﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IssueCREandTrackStatus.Contracts.Models
{
    public class Roles
    {
        [Key]
        public Int32 RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
