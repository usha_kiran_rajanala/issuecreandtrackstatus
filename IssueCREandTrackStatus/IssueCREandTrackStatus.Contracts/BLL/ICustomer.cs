﻿using System;
using System.Collections.Generic;
using System.Text;
using IssueCREandTrackStatus.Contracts.Models;
using System.Threading.Tasks;

namespace IssueCREandTrackStatus.Contracts.BLL
{
   public interface ICustomer
    {
       List<Customer> GetCustomerDetails();

        Customer GetCustomer(string id);

        bool DeleteCustomer(string id);
    }
}
