﻿using IssueCREandTrackStatus.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IssueCREandTrackStatus.Contracts.BLL
{
    public interface IUsers
    {
        List<Users> GetUsers();

        Users GetAuthenticate(string LoginId, string Password);
        Roles GetRole(Int32 RoleId);
    }
}
