﻿using IssueCREandTrackStatus.Contracts.BLL;
using IssueCREandTrackStatus.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IssueCREandTrackStatus.BLL
{
    public class UsersHelper : IUsers
    {
        private readonly ApplicationDbContext _DbAccess;
        public UsersHelper(ApplicationDbContext DbAccess)
        {
            _DbAccess = DbAccess;
        }
        public List<Users> GetUsers()
        {
            var user = _DbAccess.Users.ToList();
            return user;
        }
        public Users GetAuthenticate(string LoginId, string Password)
        {
            try
            {
                var user = _DbAccess.Users.Where(c => c.LoginId.ToLower() == LoginId.ToLower()).FirstOrDefault();
                if (user != null)
                {
                    bool flag = user.Password == Password ? true : false;
                    if (flag == true)
                        return user;
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public Roles GetRole(int RoleId)
        {
            try
            {
                var role = _DbAccess.role.Where(c => c.RoleId == RoleId).FirstOrDefault();
                if (role != null)
                    return role;
                else
                    return null;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
    }
}
