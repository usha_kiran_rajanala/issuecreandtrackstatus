﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IssueCREandTrackStatus.Contracts.BLL;
using IssueCREandTrackStatus.Contracts.Models;
using Microsoft.EntityFrameworkCore;

namespace IssueCREandTrackStatus.BLL
{
    public class CustomerHelper :ICustomer
    {
        private readonly ApplicationDbContext _DbAccess;
        public CustomerHelper(ApplicationDbContext DbAccess)
        {
            _DbAccess = DbAccess;
        }

        public List<Customer> GetCustomerDetails()
        {
            var customerDetails = _DbAccess.Customer.ToList();
            return customerDetails;
        }

        public Customer GetCustomer(string id)
        {

            if (id != null && id != "")
            {
                var customer = _DbAccess.Customer.Where(c => c.CustId == id).FirstOrDefault();
                if (customer != null)
                {
                    return customer;
                }
                else { return null; }
            }
            else { return null; }
        }

        public bool DeleteCustomer(string id)
        {

            if (id != null && id != "")
            {
                var customer = _DbAccess.Customer.Where(c => c.CustId == id).FirstOrDefault();
                if (customer != null)
                {
                    _DbAccess.Customer.Remove(customer);
                    _DbAccess.SaveChanges();
                    return true;
                }
                else { return false; }
            }
            else { return false; }
        }
    }
}
