﻿using IssueCREandTrackStatus.Contracts.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IssueCREandTrackStatus.BLL
{
   public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }
        public DbSet<IssueCREandTrackStatus.Contracts.Models.Customer> Customer { get; set; }
        public DbSet<CustOrderd> CustOrderd { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Roles> role { get; set; }
    }

    
}
